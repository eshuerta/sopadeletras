import React from 'react';

const arraysEqual = (a1,a2) => {
    return JSON.stringify(a1)==JSON.stringify(a2);
}

export const Sopas = ({ sopas, wordsFoundMap, wordsFoundAmmount }) => 
    wordsFoundMap.length > 0 && sopas.length > 0 && sopas.map((eachSopa, indexSopa) =>   
        <><div className="sopa">
            {Array.isArray(eachSopa) && eachSopa.map((sopa, indexLinea) => 
            <div className="sopaLinea">
                {Array.isArray(sopa) && sopa.map((letra, indexLetra) => { 
                return <span style={{ backgroundColor: wordsFoundMap[indexSopa].find(element => arraysEqual(element, [indexLinea, indexLetra])) ? "#ccc" : "#fff" }}>{letra} </span>
                } )}
            </div>
            )}
        </div> <span>{wordsFoundAmmount[indexSopa]} Resultados</span></>
    ) || <div> Cargando... </div>;