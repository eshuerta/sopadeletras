import React, { Component } from 'react';
import { render } from 'react-dom';
import { Sopas } from './sopas';
import { SERVER_PROTOCOL, SERVER_URL, SERVER_PORT } from './assets/constants';
import './style.css';

class App extends Component {
  constructor() {
    super();
    this.state = {
      sopas: [],
      error: false,
      wordToFind: "OIE",
      wordsFoundMap: [],
      wordsFoundAmmount: [],
    };

    // Get all sopas at load
    fetch(`${SERVER_PROTOCOL}://${SERVER_URL}:${SERVER_PORT}/api/sopas`)
      .then(response => response.json())
      .then(state => {
        if (state && state.resources) {
          this.setState({ sopas: state.resources || [] });
          this.loopSopas(state.resources);
        } else {
          this.setState({ error: true });
        }
      })
      .catch(error => {
        console.log('Hubo un problema con la petición Fetch:' + error.message);
        this.setState({ error: true })
      });
  }

  loopSopas = sopas => {
    const wordsFound = [];
    let merged = []
    const oieFoundPerSopa = [];

    sopas.map((sopa, index) => {
      const oieCoordinates = this.findOie(sopa); // arrays with location of each word in every direction founded 
      
      var filteredoieCoordinates = oieCoordinates.filter(function (el) {
        return el != null;
      });

      oieFoundPerSopa.push(filteredoieCoordinates.length); // Ammount of words found on each sopa
      wordsFound.push(filteredoieCoordinates);

      merged.push([].concat.apply([], wordsFound[index])); // flat results into one array without duplicates
    });
    
    this.setState({ wordsFoundMap: merged, wordsFoundAmmount:  oieFoundPerSopa });
  }

  findOie = (sopa) => {
    const { wordToFind } = this.state; 
    const arrayIndexes = [];
    const matrixLength = Array.isArray(sopa) && Array.isArray(sopa[0]) ? sopa.length : 1;
    const wordToFindLength = wordToFind.length;

    // loop one sopa
    sopa.map((line, lineIndex) => {
      
      const lineLength = line.length;
      let i;
      while ((i = line.indexOf("O", i + 1)) !== -1) { 
        
        const conditions = {
          up: lineIndex >= wordToFindLength - 1 && matrixLength - lineIndex >= wordToFindLength &&  wordToFindLength <= matrixLength,        
          down:  matrixLength - lineIndex >= wordToFindLength &&  wordToFindLength <= matrixLength,
          left: wordToFindLength - 1 <= i, 
          right: lineLength - i >= wordToFindLength
        }

        // if word fit vertical - up
        if (conditions.up) {
          arrayIndexes.push( this.findWord(sopa, lineIndex, i, 'up'));
        }

        // if word fit vertical - down
        if (conditions.down) {
          arrayIndexes.push( this.findWord(sopa, lineIndex, i, 'down'));
        }
        
        // if word fit horizontal - left        
        if (conditions.left) {
          arrayIndexes.push(this.findWord(sopa, lineIndex, i, 'left'));
        }

        // if word fit horizontal - right
        if (conditions.right) {
          arrayIndexes.push( this.findWord(sopa, lineIndex, i, 'right'));
        }
        
        // if word fit diagonal - up right
        if (conditions.up && conditions.right) {
          arrayIndexes.push( this.findWord(sopa, lineIndex, i, 'upRight'));
        }

        // if word fit diagonal - up left
        if (conditions.up && conditions.left) {
           arrayIndexes.push( this.findWord(sopa, lineIndex, i, 'upLeft'));
        }

        // if word fit diagonal - down right
        if (conditions.down && conditions.right) {
           arrayIndexes.push( this.findWord(sopa, lineIndex, i, 'downRight'));
        }

        // if word fit diagonal - down left
        if (conditions.down && conditions.left) {
           arrayIndexes.push( this.findWord(sopa, lineIndex, i, 'downLeft') );
        }        
      }
    });
    
    return arrayIndexes;
  }


  findWord = (sopa, lineIndex, prevLetterIndex, direction) => {

    let directionsMap = {
      up: { i1: lineIndex - 1, i2: prevLetterIndex, e1: lineIndex - 2, e2: prevLetterIndex },
      down: { i1: lineIndex + 1, i2: prevLetterIndex, e1: lineIndex + 2, e2: prevLetterIndex },
      left: { i1: lineIndex, i2: prevLetterIndex - 1, e1: lineIndex, e2: prevLetterIndex - 2 }, 
      right: { i1: lineIndex, i2: prevLetterIndex + 1, e1: lineIndex, e2: prevLetterIndex + 2 }, 
      upLeft: { i1: lineIndex - 1, i2: prevLetterIndex - 1, e1: lineIndex - 2, e2: prevLetterIndex - 2 }, 
      upRight: { i1: lineIndex - 1, i2: prevLetterIndex + 1, e1: lineIndex - 2, e2: prevLetterIndex + 2 }, 
      downLeft: { i1: lineIndex + 1, i2: prevLetterIndex - 1, e1: lineIndex + 2, e2: prevLetterIndex - 2 }, 
      downRight: { i1: lineIndex + 1, i2: prevLetterIndex + 1, e1: lineIndex + 2, e2: prevLetterIndex + 2 }, 
    }

    const checkDirection = directionsMap[direction];
    if (sopa[checkDirection.i1][checkDirection.i2] === 'I' && sopa[checkDirection.e1][checkDirection.e2] === 'E')
      return [
        [lineIndex, prevLetterIndex],
        [checkDirection.i1, checkDirection.i2],
        [checkDirection.e1, checkDirection.e2]
      ]    
  }

  render() {    
    const {sopas, wordsFoundMap, error, wordsFoundAmmount } = this.state;
    return (
      <>
        {error && <p>Ha ocurrido un error, inténtelo nuevamente</p> ||
        <div className="sopasContainer">
          
          {/*
          <h4>Sopa de letras - Test para entrevista FRONT-END</h4>
          <p>
            Se requiere hacer un <i>component</i> que segun un archivo json con unas matrices, este muestre cuantas veces aparece la palabra "OIE" dentro de ella, ya sea horizontalmente, verticalmente, o en diagonal. (En total, hay que comprobar 8 sentidos diferentes.)
          </p>
          <h4>Entrada:</h4>
          <p>La entrada consiste en seleccionar una de estas 4 matrices (proveniente de un <i>service</i> que lee un json) y mandar la seleccion embedida a una clase que haga la operación.</p>
          <p><b>NOTA:</b> Las matrices se encuentra en un archivo json localizado en: '/resources.json'. El service va a recuperar información de este archivo simulando una petición a una API.</p>
          <h4>Salida:</h4>
          <p>Para cada sopa de letras seleccionada, hay que escribir cuantas veces aparece "OIE" dentro de ella. Para esto es preferible obtener la respuesta en otro <i>componente</i> (se creativ@).</p>
          */}
          

          {wordsFoundMap.length > 0 && sopas.length > 0 && <Sopas sopas={sopas} wordsFoundMap={wordsFoundMap} wordsFoundAmmount={wordsFoundAmmount} />}
          
        </div>}
      </>
    );
  }
}

render(<App />, document.getElementById('root'));
