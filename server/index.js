const express = require('express');
const bodyParser = require('body-parser');
const pino = require('express-pino-logger')();
const data = require('../src/assets/resources.json');



const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(pino);

// fetch all sopas
app.get('/api/sopas', (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  if (data)
    res.send(JSON.stringify( data ));
  else 
    res.status(500).send([]);
});

app.listen(3001, () =>
  console.log('Express server is running on localhost:3001')
);